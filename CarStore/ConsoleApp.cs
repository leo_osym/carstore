﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore
{   
    /// <summary>
    /// Use methods to create console app of CarStore
    /// </summary>
    class ConsoleApp
    {
        public ConsoleApp() { }

        Store myCars = new Store();
        #region Console Menus
        public void AddCar()
        {
            Car myCar = new Car();
            Console.WriteLine("Please, enter the name of the car (e.g.New Honda Civic, 2018): ");
            myCar.name = Console.ReadLine();
            while (myCar.name.Length < 3)
            {
                Console.WriteLine("Wrong input!");
                myCar.name = Console.ReadLine();
            }
            Console.WriteLine("Please, enter car brand (e.g. Honda): ");
            myCar.carBrand = Console.ReadLine();
            while (myCar.carBrand.Length < 3)
            {
                Console.WriteLine("Wrong input!");
                myCar.carBrand = Console.ReadLine();
            }
            Console.WriteLine("Please, enter car model (e.g. Civic): ");
            myCar.carModel = Console.ReadLine();
            while (myCar.carModel.Length < 1)
            {
                Console.WriteLine("Wrong input!");
                myCar.carModel = Console.ReadLine();
            }
            Console.WriteLine("Please, enter the color (e.g. red): ");
            myCar.color = Console.ReadLine();
            while (myCar.color.Length < 3)
            {
                Console.WriteLine("Wrong input!");
                myCar.color = Console.ReadLine();
            }
            Console.WriteLine("Please, enter number of doors (from 2 to 6): ");
            int num;
            while (!Int32.TryParse(Console.ReadLine(), out num) || num > 6 || num < 2)
            {
                Console.WriteLine("Wrong input!");
            }
            myCar.numberOfDoors = num;
            num = 0;
            Console.WriteLine("Please, enter engine power (in h.p.): ");
            while (!Int32.TryParse(Console.ReadLine(), out num) || num > 2000 || num < 10)
            {
                Console.WriteLine("Wrong input!");
            }
            myCar.enginePower = num;
            num = 0;
            Console.WriteLine("Please, enter maximum speed of car (in kmph): ");
            while (!Int32.TryParse(Console.ReadLine(), out num) || num > 420 || num < 50)
            {
                Console.WriteLine("Wrong input!");
            }
            myCar.maxSpeed = num;
            double nn = 0;
            Console.WriteLine("Please, enter the price of the car (in $): ");
            while (!Double.TryParse(Console.ReadLine(), out nn) && nn < 1000.0 || nn > 10000000.0)
            {
                Console.WriteLine("Wrong input!");
            }
            myCar.price = nn;

            myCar.isSold = false;
            myCars.Add(myCar);
        }

        public void PrintCars(bool showSold)
        {
            Console.WriteLine("\t\t\tList of our cars:");
            Console.WriteLine("==================================================================================================================");
            Console.WriteLine("-ID--------------Item---------------Brand-------Model------Color---Engine Power--Max speed----Doors----Price--Available");
            Console.WriteLine("==================================================================================================================");
            foreach (Car c in myCars)
            {
                if (showSold == true)
                {
                    Console.Write("{0,3}  ", myCars.IndexOf(c));
                    Console.Write("{0,25}  ", c.name);
                    Console.Write("{0,10} ", c.carBrand);
                    Console.Write("{0,10} ", c.carModel);
                    Console.Write("{0,10}  ", c.color);
                    Console.Write("{0,10} ", c.enginePower);
                    Console.Write("{0,10} ", c.maxSpeed);
                    Console.Write("{0,10} ", c.numberOfDoors);
                    Console.Write(" {0,10:c0} ", c.price);
                    if (c.isSold) Console.WriteLine("{0,5}", "NO");
                    else Console.WriteLine("{0,5}", "YES");
                    Console.WriteLine("------------------------------------------------------------------------------------------------------------------");
                }
                else
                {
                    if (c.isSold == false)
                    {
                        Console.Write("{0,3}  ", myCars.IndexOf(c));
                        Console.Write("{0,25}  ", c.name);
                        Console.Write("{0,10} ", c.carBrand);
                        Console.Write("{0,10} ", c.carModel);
                        Console.Write("{0,10}  ", c.color);
                        Console.Write("{0,10} ", c.enginePower);
                        Console.Write("{0,10} ", c.maxSpeed);
                        Console.Write("{0,10} ", c.numberOfDoors);
                        Console.Write(" {0,10:c0} ", c.price);
                        if (c.isSold) Console.WriteLine("{0,5}", "NO");
                        else Console.WriteLine("{0,5}", "YES");
                        Console.WriteLine("------------------------------------------------------------------------------------------------------------------");
                    }

                }
            }

        }
        public void PrintCustomers()
        {
            Console.WriteLine("\t\t\tList of all our customers:");
            Console.WriteLine("==============================================================================");
            Console.WriteLine("---------Customer Name-------------------Item------------Brand------Model-----");
            Console.WriteLine("==============================================================================");

            foreach (Car c in myCars)
            {
                if (c.isSold)
                {
                    Console.Write("{0,20}", c.customer.CustomerName);
                    Console.Write("{0,30}  ", c.name);
                    Console.Write("{0,10} ", c.carBrand);
                    Console.WriteLine("{0,10} ", c.carModel);
                }
            }
        }
        public void BuyTheCar()
        {
            PrintCars(false);
            Console.WriteLine("==============================================================================");
            Console.WriteLine("Type index of the car which you wanna buy:");
            int num;
            while (!Int32.TryParse(Console.ReadLine(), out num))
            {
                Console.WriteLine("Wrong input!");
            }
            try
            {
                if (myCars[num].isSold == false)
                {
                    myCars[num].isSold = true;
                    Customer cust = new Customer();
                    Console.WriteLine("Please, enter your first name:");
                    cust.firstName = Console.ReadLine();
                    while (cust.firstName.Length < 3)
                    {
                        Console.WriteLine("Wrong input!");
                        cust.firstName = Console.ReadLine();
                    }
                    Console.WriteLine("Please, enter your last name:");
                    cust.lastName = Console.ReadLine();
                    while (cust.lastName.Length < 3)
                    {
                        Console.WriteLine("Wrong input!");
                        cust.lastName = Console.ReadLine();
                    }
                    myCars[num].customer = cust;
                    Console.WriteLine($"Congratulations, mr. {cust.lastName}! This {myCars[num].carBrand} {myCars[num].carModel} is a good choice!");
                }
                else Console.WriteLine("Error! Can't sell this car!");
            }
            catch { Console.WriteLine("Error! Can't sell this car!"); }


        }

        public void MainPage()
        {
            //picture was taken from this site: https://www.asciiart.eu/vehicles/cars
            Console.WriteLine("==============================================================================");
            Console.WriteLine("\t\t\t  Welcome to my Super Cool Car Store!");
            Console.WriteLine(@"
                                     _______
                                    //  ||\ \
                               _ __//___||_\ \___
                              )  _          _    \
                              |_/ \________/ \___|
                            _ __\_/________\_/______");
        }
        public void SmallMenu()
        {
            Console.WriteLine("==============================================================================");
            Console.WriteLine("Enter any number from the list below to navigate");
            Console.WriteLine("1 - Add car for selling");
            Console.WriteLine("2 - Show all cars in the store");
            Console.WriteLine("3 - Show all available cars in the store");
            Console.WriteLine("4 - Show all our customers");
            Console.WriteLine("5 - Buy the car");
            Console.WriteLine("6 - Return to the main menu");
            Console.WriteLine("7 - Exit");
        }

        #endregion

    }
}
