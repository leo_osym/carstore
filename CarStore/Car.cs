﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore
{
    /// <summary>
    /// Create a car with all the features
    /// </summary>
    class Car
    {
        #region Properties
        private int EnginePower;
        private int MaxSpeed;
        private double Price;
        public string name { get; set; }
        public int numberOfDoors { get; set; }
        public string carBrand { get; set; }
        public string carModel { get; set; }
        public bool isSold { get; set; }
        public string color { get; set; }
        public int enginePower {
            get
            {
                    return EnginePower;
            }
            set
            {
                if (value < 10 || value > 2000) EnginePower = 100;
                else EnginePower = value;
            }
        }
        public int maxSpeed
        {
            get
            {
                return MaxSpeed;
            }
            set
            {
                if (value < 50 || value > 420) MaxSpeed = 180;
                else MaxSpeed = value;
            }
        }
        public double price
        {
            get
            {
                return Price;
            }
            set
            {
                if (value < 100 || value > 10000000) Price = 30000;
                else Price = value;
            }
        }
        public Customer customer { get; set; }
        #endregion
        #region Constructors
        public Car()
        {

            name = "Fiat Tipo, 2017";
            numberOfDoors = 5;
            carBrand = "Fiat";
            carModel = "Tipo";
            isSold = false;
            color = "blue";
            enginePower = 200;
            maxSpeed = 230;
            price = 22000.0;
            customer = new Customer();
        }
        public Car(string name, string carBrand = "Fiat", string carModel = "Tipo", string color = "Red", int numberOfDoors = 5, bool isSold = false, int enginePower = 95, int maxSpeed = 200, double price = 10000.0)
            :this (new Customer(), name, carBrand, carModel: carModel, color: color, numberOfDoors: numberOfDoors, isSold: isSold, enginePower: enginePower, maxSpeed: maxSpeed, price: price)
        {
           this.customer = new Customer();
        }
        public Car(Customer customer, string name, string carBrand="Fiat", string carModel="Tipo", string color="Red", int numberOfDoors=5, bool isSold=false,  int enginePower=95, int maxSpeed=200, double price=10000.0)
        {
            if (numberOfDoors > 5 || numberOfDoors < 2) numberOfDoors=5;
            if (enginePower < 10 || enginePower > 2000) enginePower = 100;
            if (maxSpeed < 50 || maxSpeed > 420) maxSpeed = 180;
            if (price < 100 || price > 10000000) price = 30000; 
            this.name = name;
            this.numberOfDoors = numberOfDoors;
            this.carBrand = carBrand;
            this.carModel = carModel;
            this.color = color;
            this.enginePower = enginePower;
            this.maxSpeed = maxSpeed;
            this.price = price;
            this.isSold = isSold;
            if(isSold == true)
            {
                this.customer = customer;
            }
        }
        #endregion
    }
}
