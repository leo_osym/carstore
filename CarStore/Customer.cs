﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore
{
    /// <summary>
    /// Create a customer which can buy a car
    /// </summary>
    class Customer
    {
        private string FirstName;
        private string LastName;

        public string CustomerName
        {
            get => String.Format($"{FirstName} {LastName}");
        }
        public string firstName { get => FirstName; set => FirstName = value; }
        public string lastName { get => LastName; set => LastName = value; }

        public Customer()
        {
            FirstName = "Unknown";
            LastName = "Unknown";
        }
        public Customer(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
        public void Print()
        {
            Console.WriteLine($"{FirstName} {LastName}");
        }
        
    }
}
