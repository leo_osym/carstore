﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore
{   
    /// <summary>
    /// Represents list of <Car> objects
    /// </summary>
    class Store: IEnumerable<Car>, IList<Car>
    {
        private List<Car> myCars = new List<Car> { };

        #region Constructor
        public Store()
        {
            myCars.Add(new Car());
            myCars.Add(new Car("New Seat Cordoba","Seat","Cordoba","green",5,false,120,200,12000.0));
            myCars.Add(new Car(new Customer("Aja","Volkman"),"Used Tesla Model X, 2018","Tesla","Model X","red", 500, true,250,5,200000.0));
            myCars.Add(new Car(new Customer("Taylor", "York"), "New Infiniti Q50", "Infiniti", "Q50", "darkblue", 300, true, 250, 5, 27000.0));
            myCars.Add(new Car( "Used Volkswagen Polo, 2017", "Volkswagen", "Polo", "gray", 230, false, 230, 5, 13000.0));
            myCars.Add(new Car("New Audi A8, 2017","Audi", "A8", "black", 340, false, 300, 5, 150000.0));

            Car car2 = new Car();
            car2.name = "Renault Megane, 2018";
            car2.carBrand = "Renault";
            car2.carModel = "Megane";
            car2.color = "gray";
            car2.enginePower = 115;
            car2.isSold = true;
            car2.maxSpeed = 240;
            car2.numberOfDoors = 5;
            car2.price = 22000.0;
            car2.customer = new Customer("Leonid", "Osym");
            myCars.Add(car2);   
            
        }
        #endregion

        #region IEnumerable<Car>, IList<Car> properties
        public Car this[int index] { get => ((IList<Car>)myCars)[index]; set => ((IList<Car>)myCars)[index] = value; }

        public int Count => ((IList<Car>)myCars).Count;

        public bool IsReadOnly => ((IList<Car>)myCars).IsReadOnly;

        public void Add(Car item)
        {
            ((IList<Car>)myCars).Add(item);
        }

        public void Clear()
        {
            ((IList<Car>)myCars).Clear();
        }

        public bool Contains(Car item)
        {
            return ((IList<Car>)myCars).Contains(item);
        }

        public void CopyTo(Car[] array, int arrayIndex)
        {
            ((IList<Car>)myCars).CopyTo(array, arrayIndex);
        }

        public IEnumerator<Car> GetEnumerator()
        {
            return ((IEnumerable<Car>)myCars).GetEnumerator();
        }

        public int IndexOf(Car item)
        {
            return ((IList<Car>)myCars).IndexOf(item);
        }

        public void Insert(int index, Car item)
        {
            ((IList<Car>)myCars).Insert(index, item);
        }

        public bool Remove(Car item)
        {
            return ((IList<Car>)myCars).Remove(item);
        }

        public void RemoveAt(int index)
        {
            ((IList<Car>)myCars).RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Car>)myCars).GetEnumerator();
        }
        #endregion
    }
}
