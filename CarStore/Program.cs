﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore
{
    class Program
    {

        static void Main(string[] args)
        {
            ConsoleApp app = new ConsoleApp();
            app.MainPage();
            app.SmallMenu();
            while (true)
            {
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.D1:
                        Console.Clear();
                        app.AddCar();
                        app.SmallMenu();
                        break;
                    case ConsoleKey.D2:
                        Console.Clear();
                        app.PrintCars(true);
                        app.SmallMenu();
                        break;
                    case ConsoleKey.D3:
                        Console.Clear();
                        app.PrintCars(false);
                        app.SmallMenu();
                        break;
                    case ConsoleKey.D4:
                        Console.Clear();
                        app.PrintCustomers();
                        app.SmallMenu();
                        break;
                    case ConsoleKey.D5:
                        Console.Clear();
                        app.BuyTheCar();
                        app.SmallMenu();
                        break;
                    case ConsoleKey.D6:
                        Console.Clear();
                        app.MainPage();
                        app.SmallMenu();
                        break;
                    case ConsoleKey.D7:
                        Console.WriteLine();
                        Environment.Exit(0);
                        continue;
                }

            }
        }
    }
}
